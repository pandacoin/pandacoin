
Pandacoin Official Development Repo
==================================

### Pandacoin Resources
* Client and Source:
[Client Binaries](http://pandacoin.tech/wallet),
[Source Code](https://gitlab.com/pandacoin/pandacoin)
* Documentation: [Pandacoin Whitepaper](https://pandacoin.tech/resources#whitepaper),
[Pandacoin Docs](https://wiki.pandacoin.tech)
* Help: 
[Telegram](https://t.me/pandacoin)

-------

This class is aligned with the JSON standard, [RFC
7159](https://tools.ietf.org/html/rfc7159.html).

## Library usage

This is a fork of univalue used by Bitcoin Core. It is not maintained for usage
by other projects. Notably, the API may break in non-backward-compatible ways.

Other projects looking for a maintained library should use the upstream
univalue at https://github.com/jgarzik/univalue.
